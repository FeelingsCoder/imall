﻿using IMall.Web.Models.ViewModels.Manager;
using IMall.Web.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace IMall.Web.Service
{
    public class ManagerService
    {
        Models.IMallDbContext context = new Models.IMallDbContext();

        /// <summary>
        /// 获取订单列表
        /// </summary>
        /// <param name="state"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public OrderList GetOrderList(int state, int page)
        {
            OrderList result = new OrderList();
            var query = context.Order.Include("User").Include("Commodity").AsQueryable<Models.Order>();
            if (state != -1) query = query.Where(p => p.State == state);
            else
            {
                query = query.Where(p => p.State == 2 || p.State == 3 || p.State == 4);
            }
            query = query.OrderByDescending(p => p.CreatedTime);
            var list = query
                .Skip(result.PageSize * (page - 1))
                .Take(result.PageSize)
                .Select(p => new Order()
                {
                    Id = p.Id,
                    State = p.State,
                    CommodityId = p.CommodityId,
                    CommodityName = p.Commodity.Name,
                    CreatedTime = p.CreatedTime,
                    Number = p.Number,
                    Price = p.Price,
                    UnitPrice = p.Price,
                    UserName = p.User.Name,
                }).ToList();

            result.List = list;
            result.Page = page;
            result.Number = query.Count();
            return result;
        }

        public CommodityInfo GetCommodity(string id)
        {
            return context.Commodity.Where(p => p.Id == id).Select(p => new CommodityInfo()
            {
                Id = p.Id,
                Freight = p.Freight,
                Icon = p.Icon,
                Name = p.Name,
                Number = p.Number,
                Price = p.Price,
                Type = p.Type
            }).First();
        }

        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="fileStream"></param>
        public Result UploadFile(HttpPostedFileBase file)
        {
            Result result = new Result();
            string name = Guid.NewGuid().ToString().ToUpper() + ".jpg";
            string path = "D:\\项目\\GIT\\IMall\\IMall商城\\IMall.Web\\Content\\List\\";

            file.SaveAs(path + name);
            result.message = "上传成功";
            result.state = "ok";
            result.attach = name;

            return result;
        }

        /// <summary>
        /// 保存商品信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Result SaveCommodity(CommodityInfo model)
        {
            Result result = new Result();
            if (model.Id == null)
            {
                context.Commodity.Add(new Models.Commodity()
                {
                    CreatedTime = DateTime.Now,
                    State = 2,
                    Freight = model.Freight,
                    Icon = model.Icon,
                    Id = Guid.NewGuid().ToString(),
                    Name = model.Name,
                    Number = model.Number,
                    Price = model.Price,
                    Type = model.Type,
                });
            }
            else
            {
                var c = context.Commodity.Where(p => p.Id == model.Id).First();
                c.Number = model.Number;
                c.Price = model.Price;
                c.Type = model.Type;
                c.Freight = model.Freight;
                if(model.Icon != null) c.Icon = model.Icon;
                c.Name = model.Name;
            }
            context.SaveChanges();
            result.state = "ok";
            result.message = "保存成功";
            return result;
        }

        /// <summary>
        /// 获取商品列表
        /// </summary>
        /// <param name="state"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public CommodityList GetCommodityList(int state, int page, int type)
        {
            CommodityList result = new CommodityList();
            var query = context.Commodity.Where(p => p.State == state);
            if (type == 1) query = query.Where(p => p.Type == "男装");
            else if (type == 2) query = query.Where(p => p.Type == "女装");
            else if (type == 3) query = query.Where(p => p.Type == "玩具");
            query = query.OrderByDescending(p => p.CreatedTime);
            var list = query
                .Skip(result.PageSize * (page - 1))
                .Take(result.PageSize)
                .Select(p => new Commodity()
                {
                    Id = p.Id,
                    Name = p.Name,
                    Number = p.Number,
                    Price = p.Price,
                    State = p.State
                }).ToList();

            result.List = list;
            result.Page = page;
            result.Number = query.Count();
            return result;
        }

        /// <summary>
        /// 获取订单详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public OrderDetail GetOrderDetail(string id)
        {
            var order = context.Order.Where(p => p.Id == id).FirstOrDefault();
            var command = context.Commodity.Where(p => p.Id == order.CommodityId).FirstOrDefault();
            var adress = context.Adress.Where(p => p.Id == order.AdressId).FirstOrDefault();
            OrderDetail model = new OrderDetail()
            {
                Adress = AdressHelper.AdressToString(adress),
                AdressId = order.AdressId,
                CommandityPrice = command.Price,
                CommandityId = command.Id,
                CommandityIcon = command.Icon,
                CommandityFreight = command.Freight,
                CommandityName = command.Name,
                CompleteTime = order.CompleteTime,
                CreatedTime = order.CreatedTime,
                Id = order.Id,
                Number = order.Number,
                PaymentTime = order.PaymentTime,
                Price = order.Price,
                Size = order.Size,
                State = order.State,
            };

            return model;
        }

        /// <summary>
        /// 下架
        /// </summary>
        /// <param name="id"></param>
        public void RemoveCommodity(string id)
        {
            var commodity = context.Commodity.Where(p => p.Id == id).First();
            if (commodity != null && commodity.State == 1)
            {
                commodity.State = 2;
            }
            context.SaveChanges();
        }

        /// <summary>
        /// 上架
        /// </summary>
        /// <param name="id"></param>
        public void AddCommodity(string id)
        {
            var commodity = context.Commodity.Where(p => p.Id == id).First();
            if (commodity != null && commodity.State == 2)
            {
                commodity.State = 1;
            }
            context.SaveChanges();
        }

        /// <summary>
        /// 发货
        /// </summary>
        /// <param name="id"></param>
        public void Deliver(string id)
        {
            var order = context.Order.Where(p => p.Id == id).First();
            if(order != null && order.State == 2)
            {
                order.State = 3;
            }
            context.SaveChanges();
        }
    }
}