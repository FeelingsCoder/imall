﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMall.Web.Tools
{
    /// <summary>
    /// 返回信息
    /// </summary>
    public class Result
    {
        /// <summary>
        /// 状态
        /// </summary>
        public string state { get; set; }
        /// <summary>
        /// 返回消息
        /// </summary>
        public string message { get; set; }
        /// <summary>
        /// 附加信息
        /// </summary>
        public object attach { get; set; }
    }
}