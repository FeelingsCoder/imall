﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMall.Web.Tools
{
    /// <summary>
    /// 分页基础模型
    /// </summary>
    public class Paging
    {
        /// <summary>
        /// 当前页码
        /// </summary>
        public int Page { get; set; }
        
        /// <summary>
        /// 每页记录数
        /// </summary>
        public int PageSize { get; set; } = 20;

        /// <summary>
        /// 记录数
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// 页数
        /// </summary>
        public int PageNumber
        {
            get { return Number / PageSize + (Number % PageSize == 0 ? 0 : 1); }
        }


    }
}