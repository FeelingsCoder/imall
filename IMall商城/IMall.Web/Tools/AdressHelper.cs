﻿using IMall.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMall.Web.Tools
{
    public static class AdressHelper
    {
        public static string AdressToString(Adress item)
        {
            return item.Name + " " + item.PhoneNumber + " " + item.Province + "省" + item.City + "市" + item.Location;
        }
    }
}