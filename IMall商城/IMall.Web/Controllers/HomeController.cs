﻿using IMall.Web.Fillters;
using IMall.Web.Models;
using IMall.Web.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IMall.Web.Controllers
{
    public class HomeController : Controller
    {
        IMallService service = new IMallService();

        /// <summary>
        /// 网站首页
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            //context.Adress.AsQueryable().Any();
            return View();
        }

        /// <summary>
        /// 首页幻灯片
        /// </summary>
        /// <returns></returns>
        public ActionResult Slide()
        {
            return View();
        }

        /// <summary>
        /// 首页推荐
        /// </summary>
        /// <returns></returns>
        public ActionResult Recommend()
        {
            return View();
        }

        /// <summary>
        /// 商品列表页
        /// </summary>
        /// <returns></returns>
        [LoginFillter]
        public ActionResult List(int page = 1, string keyword = null)
        {
            var list = service.GetListModel(page, keyword);
            return View(list);
        }

        /// <summary>
        /// 商品信息页
        /// </summary>
        /// <returns></returns>
        [LoginFillter]
        public ActionResult Details(string id)
        {
            var model = service.GetCommodity(id, (Session["User"] as User).Id);
            return View(model);
        }

        /// <summary>
        /// 结算页面
        /// </summary>
        /// <returns></returns>
        [LoginFillter]
        public ActionResult Settlement(string id)
        {
            var model = service.GetOrder(id);
            return View(model);
        }

        /// <summary>
        /// 提交订单
        /// </summary>
        /// <param name="size">尺寸</param>
        /// <param name="adress">地址</param>
        /// <returns></returns>
        [LoginFillter]
        [HttpPost]
        public ActionResult Settlement(string id, string size, string adress)
        {
            string orderId = service.CreateOrder(size, adress,(Session["User"] as User).Id, id);
            var model = service.GetOrder(orderId);
            return View(model);
        }

        /// <summary>
        /// 支付
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [LoginFillter]
        [HttpPost]
        public JsonResult Pay(string id)
        {
            service.Pay(id);
            return Json("Ok");
        }

        /// <summary>
        /// 取消订单
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [LoginFillter]
        [HttpPost]
        public JsonResult Cancel(string id)
        {
            service.Cancel(id);
            return Json("Ok");
        }

        /// <summary>
        /// 确认收货
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [LoginFillter]
        [HttpPost]
        public JsonResult Complete(string id)
        {
            service.Complete(id);
            return Json("Ok");
        }

        /// <summary>
        /// 历史订单页面
        /// </summary>
        /// <returns></returns>
        [LoginFillter]
        public ActionResult History()
        {
            var model = service.GetHistory((Session["User"] as User).Id);
            return View(model);
        }
        /// <summary>
        /// 登陆表单提交
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Login(User user)
        {
            var u = service.Login(user);
            if (u != null)
            {
                if (Session["User"] == null)
                {
                    Session.Add("User", u);
                }
                else Session["User"] = u;
            }
            return RedirectToAction("History");
        }
        
    }
}