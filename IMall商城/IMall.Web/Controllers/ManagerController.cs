﻿using IMall.Web.Models.ViewModels.Manager;
using IMall.Web.Service;
using IMall.Web.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IMall.Web.Controllers
{
    public class ManagerController : Controller
    {
        ManagerService service = new ManagerService();

        /// <summary>
        /// 卖家端 首页
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 获取订单列表
        /// </summary>
        /// <returns></returns>
        public JsonResult GetOrders(int? state, int? page)
        {
            var model = service.GetOrderList(state ?? -1, page ?? 1);
            return Json(model);
        }

        /// <summary>
        /// 订单详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Detail(string id)
        {
            var model = service.GetOrderDetail(id);
            return View(model);
        }

        /// <summary>
        /// 发货
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Deliver(string id)
        {
            service.Deliver(id);
            return Json("ok");
        }

        /// <summary>
        /// 商品列表页
        /// </summary>
        /// <returns></returns>
        public ActionResult Commodity()
        {
            return View();
        }

        /// <summary>
        /// 添加或者编辑商品
        /// </summary>
        /// <returns></returns>
        public ActionResult EditCommodity(string id)
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                return View(service.GetCommodity(id));
            }
            return View(new CommodityInfo());
        }

        /// <summary>
        /// 表单提交
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditCommodity(CommodityInfo model)
        {
            if (Session["FileName"] != null) model.Icon = Session["FileName"].ToString();
            Result result = service.SaveCommodity(model);
            return RedirectToAction("Commodity");
        }

        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public JsonResult UploadFile(HttpPostedFileBase file)
        {
            var result = service.UploadFile(file);
            if (Session["FileName"] == null) Session.Add("FileName", result.attach);
            else Session["FileName"] = result.attach;
            return Json(result);
        }

        /// <summary>
        /// 获取商品列表
        /// </summary>
        /// <returns></returns>
        public JsonResult GetCommodityList(int? state, int? page, int? type)
        {
            var model = service.GetCommodityList(state ?? 1, page ?? 1, type??0);
            return Json(model);
        }

        /// <summary>
        /// 上架
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AddCommodity(string id)
        {
            service.AddCommodity(id);
            return Json("ok");
        }

        /// <summary>
        /// 下架
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveCommodity(string id)
        {
            service.RemoveCommodity(id);
            return Json("ok");
        }
    }
}