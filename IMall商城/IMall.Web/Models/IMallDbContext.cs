﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace IMall.Web.Models
{
    /// <summary>
    /// 数据库上下文
    /// </summary>
    public class IMallDbContext : DbContext
    {
        /// <summary>
        /// 用户信息表
        /// </summary>
        public DbSet<User> User { get; set; }

        /// <summary>
        /// 地址表
        /// </summary>
        public DbSet<Adress> Adress { get; set; }

        /// <summary>
        /// 商品表
        /// </summary>
        public DbSet<Commodity> Commodity { get; set; }

        /// <summary>
        /// 订单表
        /// </summary>
        public DbSet<Order> Order { get; set; }
        

        #region 加载实体映射

        /// <summary>
        /// 方法重写
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //加载实体映射
            //modelBuilder.Configurations.Add(new BsLineRepairRangeMap());

            modelBuilder.Properties().Where(p => p.Name == "RowVersion" || p.Name == "RowVersion".ToUpper()).Configure(p => p.IsConcurrencyToken());
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();//移除复数表名的契约

            //modelBuilder.Types().Configure(f => f.ToTable(("t_" + f.ClrType).ToUpper(Infran.GetDatabaseType()));  
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();  //表中都统一设置禁用一对多级联删除
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>(); //表中都统一设置禁用多对多级联删除
            base.OnModelCreating(modelBuilder);
        }
        #endregion
    }
}