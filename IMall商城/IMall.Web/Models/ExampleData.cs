﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace IMall.Web.Models
{
    public class ExampleData : DropCreateDatabaseAlways<IMallDbContext>
    {
        protected override void Seed(IMallDbContext context)
        {
            List<User> users = new List<User>()
            {
                new User()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "admin",
                    Password = "admin"
                }
            };

            List<Adress> adress = new List<Adress>()
            {
                new Adress()
                {
                    City = "青岛",
                    Province = "山东",
                    Id = Guid.NewGuid().ToString(),
                    Name = "王广伟",
                    Location = "宁夏路308号青岛大学",
                    PhoneNumber = "18300000000",
                    UserId = users.First().Id
                }
            };

            List<Commodity> commodity = new List<Commodity>()
            {
                new Commodity()
                {
                    Id = Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon = "05561a07-75ae-44b2-a05e-eac4e31edde0_235x297_90.jpg",
                    Name = "时尚绣花修身休闲短袖Polo衫[商场同款]白色",
                    Price = 149,
                    Type = "男装",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id = Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon = "5a76c62c-a4df-42c7-84b3-6b5072832031_235x297_90.jpg",
                    Name = "时尚印花休闲圆领短袖T恤[商场同款]黑色",
                    Price = 169,
                    Type = "男装",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },

                new Commodity()
                {
                    Id = Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon = "b7ce9a28-5619-49bd-9e1c-c40813986e2c_235x297_90.jpg",
                    Name = "时尚绣花修身休闲短袖Polo衫[商场同款]黑色",
                    Price = 149,
                    Type = "男装",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id = Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon = "f3f43296-a361-4480-853b-fda840bd1305_235x297_90.jpg",
                    Name = "时尚绣花休闲修身长袖衬衫[商场同款]白色",
                    Price = 249,
                    Type = "男装",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id = Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon = "eaee5c58-d738-44a0-88f9-12f0a7b42849_235x297_90.jpg",
                    Name = "时尚绣花休闲修身长袖衬衫[商场同款]黑色",
                    Price = 249,
                    Type = "男装",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id = Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon = "5da1de9c-d10c-4206-843b-599612e9eadc_235x297_90.jpg",
                    Name = "玫瑰数码印花修身长袖衬衫[商场同款]黑白条色",
                    Price = 249,
                    Type = "男装",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id = Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon = "748cdf9f-31a8-4574-a088-02458045ad12_235x297_90.jpg",
                    Name = "时尚绣花修身休闲短袖Polo衫[商场同款]紫色",
                    Price = 149,
                    Type = "男装",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id = Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon = "a95b7a37-d941-4525-be74-7837dc44bdbb_235x297_90.jpg",
                    Name = "时尚印花拼接修身短袖T恤[商场同款]黑色",
                    Price = 149,
                    Type = "男装",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id=Guid.NewGuid().ToString(),
                    Freight = 0,
                    Name = "红色长袖外套",
                    Price = 116,
                    Icon="97f38370-4ef6-421b-9b3b-aa4f26a04e90_235x297_90.jpg",
                    Type = "女装",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id=Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon="6561609773-5_235x297_90.jpg",
                    Name = "PSALTER绒毛翻领潮流外套",
                    Price = 856,
                    Type = "女装",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id=Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon="16a423e1-3423-4102-ac4e-9b581233cbde_235x297_90.jpg",
                    Name = "时尚印花长袖外套",
                    Price = 189,
                    Type = "女装",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id=Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon="99228399-3b0e-4d0d-9bee-c186959df3a7_235x297_90.jpg",
                    Name = "黑色长袖时尚小外套",
                    Price = 278,
                    Type = "女装",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id=Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon="3e539c26-a575-4529-845a-4404463bc679_235x297_90.jpg",
                    Name = "枪色潮流金属质感棒球外套",
                    Price = 329,
                    Type = "女装",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id=Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon="1587599-5_235x297_90.jpg",
                    Name = "时尚薄款牛仔休闲短款翻领外套",
                    Price = 154,
                    Type = "女装",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id=Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon="d76b8980-947f-4ebf-8f64-6a58f26ccdfc_235x297_90.jpg",
                    Name = "时尚七分袖外套",
                    Price = 215,
                    Type = "女装",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id=Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon="833a10db-e64f-403f-a4c7-6e5ea0d366b3_235x297_90.jpg",
                    Name = "女款时尚百搭修身薄款中长款风衣",
                    Price = 189,
                    Type = "女装",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id=Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon="5310702042-5_235x297_90.jpg",
                    Name = "针织百搭短款修身披肩毛衫",
                    Price = 118,
                    Type = "女装",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id=Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon="a201a8d3-6413-41e7-869d-2819d188158b_235x297_90.jpg",
                    Name = "【唐艺昕明星同款】军绿色长袖夹克外套短款",
                    Price = 178,
                    Type = "女装",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },//
                new Commodity()
                {
                    Id=Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon="33cae41f-d0a0-45d7-9c31-d9c13c3a04a3_235x297_90.jpg",
                    Name = "费雪小象F0202",
                    Price = 29,
                    Type = "玩具",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id=Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon="abebb9ce-f3ea-42c5-811c-94294abb534b_235x297_90.jpg",
                    Name = "费雪 18寸跳跳球黄色F0701H3",
                    Price = 37,
                    Type = "玩具",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id=Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon="f41dfccf-9fa0-4ab9-b807-0e41f3edb575_235x297_90.jpg",
                    Name = "风火轮风火轮电动多功能汽车世界",
                    Price = 221,
                    Type = "玩具",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id=Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon="2e0e67ab-a92b-4a7e-b250-b2137e30386c_235x297_90.jpg",
                    Name = "托马斯&朋友 电动系列之蓝山轨道套装",
                    Price = 150,
                    Type = "玩具",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id=Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon="775b94b5-45d5-4832-b297-d5ddfd8f3682_235x297_90.jpg",
                    Name = "托马斯&朋友 电动系列之狄赛尔能量之旅轨道套装 CDV10",
                    Price = 173,
                    Type = "玩具",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
                new Commodity()
                {
                    Id=Guid.NewGuid().ToString(),
                    Freight = 0,
                    Icon="36ed3e5d-c39c-48c2-9bc3-61dda72da8d5_235x297_90.jpg",
                    Name = "费雪 美高大袋装积木80片粉色DCH62",
                    Price = 88,
                    Type = "玩具",
                    State = 1,
                    CreatedTime = DateTime.Now,
                    Number = 100,
                },
            };

            foreach (var user in users)
            {
                context.User.Add(user);
            }

            foreach (var c in commodity)
            {
                context.Commodity.Add(c);
            }

            foreach (var a in adress)
            {
                context.Adress.Add(a);
            }
            

        }
    }
}