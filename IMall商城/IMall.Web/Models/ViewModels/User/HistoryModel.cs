﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMall.Web.Models.ViewModels
{
    /// <summary>
    /// 历史订单模型
    /// </summary>
    public class HistoryModel
    {
        /// <summary>
        /// 订单编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 外键：用户ID
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 外键：商品编号
        /// </summary>
        public string CommodityId { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        public string CommodityName { get; set; }

        /// <summary>
        /// 订单创建时间
        /// </summary>
        public DateTime? CreatedTime { get; set; }
        
        /// <summary>
        /// 商品数量
        /// </summary>
        public int? Number { get; set; }

        /// <summary>
        /// 订单总价
        /// </summary>
        public decimal? Price { get; set; }

        /// <summary>
        /// 订单状态
        /// </summary>
        public int State { get; set; }

        public string StateText
        {
            get
            {
                string text = null;
                switch (State)
                {
                    case 1: //创建完成 待付款
                        text = "待付款";
                        break;
                    case 2: //待发货
                        text = "待发货";
                        break;
                    case 3: //待收货
                        text = "待收货";
                        break;
                    case 4: //订单完成
                        text = "订单完成";
                        break;
                    case 0: //订单取消
                        text = "订单取消";
                        break;
                }
                return text;
            }
        }
    }
}