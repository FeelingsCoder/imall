﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMall.Web.Models.ViewModels
{
    /// <summary>
    /// 商品信息模型
    /// </summary>
    public class CommodityModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
        public string Icon { get; set; }
        public string Freight { get; set; }
        public string Infomation { get; set; } = "商家未填写任何商品详情。";

        public List<KeyValuePair<string, string>> Adress { get; set; }
    }
}