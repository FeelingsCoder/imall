﻿using IMall.Web.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMall.Web.Models.ViewModels
{
    /// <summary>
    /// 列表页面模型
    /// </summary>
    public class ListModel : Paging
    {
        /// <summary>
        /// 记录集合
        /// </summary>
        public List<ListItemModel> List { set; get; }

        /// <summary>
        /// 关键字
        /// </summary>
        public string KeyWord { get; set; }
    }
}