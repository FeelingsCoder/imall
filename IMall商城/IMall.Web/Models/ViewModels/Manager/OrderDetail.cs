﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMall.Web.Models.ViewModels.Manager
{
    /// <summary>
    /// 订单详情
    /// </summary>
    public class OrderDetail
    {
        /// <summary>
        /// 订单编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        public string Adress { get; set; }

        /// <summary>
        /// 地址编号
        /// </summary>
        public string AdressId { get; set; }

        /// <summary>
        /// 商品编号
        /// </summary>
        public string CommandityId { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        public string CommandityName { get; set; }

        /// <summary>
        /// 商品单价
        /// </summary>
        public decimal? CommandityPrice { get; set; }

        /// <summary>
        /// 商品图片
        /// </summary>
        public string CommandityIcon { get; set; }

        /// <summary>
        /// 运费
        /// </summary>
        public decimal? CommandityFreight { get; set; }

        /// <summary>
        /// 订单创建时间
        /// </summary>
        public DateTime? CreatedTime { get; set; }
        /// <summary>
        /// 订单付款时间
        /// </summary>
        public DateTime? PaymentTime { get; set; }
        /// <summary>
        /// 订单成交时间
        /// </summary>
        public DateTime? CompleteTime { get; set; }

        /// <summary>
        /// 商品尺寸
        /// </summary>
        public string Size { get; set; }

        /// <summary>
        /// 商品数量
        /// </summary>
        public int? Number { get; set; }

        /// <summary>
        /// 订单总价
        /// </summary>
        public decimal? Price { get; set; }

        /// <summary>
        /// 订单状态
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// 状态样式
        /// </summary>
        public string StateCss
        {
            get
            {
                string css = null;
                switch (State)
                {
                    case 1: //创建完成 待付款
                        css = "alert-info";
                        break;
                    case 2: //待发货
                    case 3: //待收货
                        css = "alert-warning";
                        break;
                    case 4: //订单完成
                        css = "alert-success";
                        break;
                    case 0: //订单取消
                        css = "alert-danger";
                        break;
                }
                return css;
            }
        }

        public string StateText
        {
            get
            {
                string text = null;
                switch (State)
                {
                    case 1: //创建完成 待付款
                        text = "创建订单成功，待付款";
                        break;
                    case 2: //待发货
                        text = "支付完成，待发货";
                        break;
                    case 3: //待收货
                        text = "待收货";
                        break;
                    case 4: //订单完成
                        text = "交易完成";
                        break;
                    case 0: //订单取消
                        text = "订单已取消";
                        break;
                }
                return text;
            }
        }
    }
}