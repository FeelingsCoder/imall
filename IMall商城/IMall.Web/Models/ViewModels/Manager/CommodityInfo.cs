﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMall.Web.Models.ViewModels.Manager
{
    /// <summary>
    /// 商品信息
    /// </summary>
    public class CommodityInfo
    {
        /// <summary>
        /// 商品编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 商品类别
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 商品单价
        /// </summary>
        public decimal? Price { get; set; }

        /// <summary>
        /// 商品图片
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 运费
        /// </summary>
        public decimal? Freight { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int? Number { get; set; }
    }
}