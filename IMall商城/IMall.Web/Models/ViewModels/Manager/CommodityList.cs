﻿using IMall.Web.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMall.Web.Models.ViewModels.Manager
{
    public class CommodityList : Paging
    {
        public List<Commodity> List { set; get; }
    }
}