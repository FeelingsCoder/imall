﻿using IMall.Web.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMall.Web.Models.ViewModels.Manager
{
    /// <summary>
    /// 订单列表
    /// </summary>
    public class OrderList : Paging
    {
        /// <summary>
        /// 订单列表
        /// </summary>
        public List<Order> List { get; set; }
    }
}