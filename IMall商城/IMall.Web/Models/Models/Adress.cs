﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMall.Web.Models
{
    /// <summary>
    /// 地址模型
    /// </summary>
    public class Adress
    {
        /// <summary>
        /// 地址编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 外键：用户Id
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 导航属性：用户信息
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 省份
        /// </summary>
        public string Province { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string Location { get; set; }
    }
}