﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMall.Web.Models
{
    /// <summary>
    /// 订单模型
    /// </summary>
    public class Order
    {
        /// <summary>
        /// 订单编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 外键：用户ID
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 导航属性：用户信息
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// 外键：商品编号
        /// </summary>
        public string CommodityId { get; set; }
        /// <summary>
        /// 导航属性：商品信息
        /// </summary>
        public Commodity Commodity { get; set; }

        /// <summary>
        /// 外键：地址编号
        /// </summary>
        public string AdressId { get; set; }
        /// <summary>
        /// 导航属性：地址信息
        /// </summary>
        public Adress Adress { get; set; }

        /// <summary>
        /// 订单创建时间
        /// </summary>
        public DateTime? CreatedTime { get; set; }
        /// <summary>
        /// 订单付款时间
        /// </summary>
        public DateTime? PaymentTime { get; set; }
        /// <summary>
        /// 订单成交时间
        /// </summary>
        public DateTime? CompleteTime { get; set; }

        /// <summary>
        /// 商品尺寸
        /// </summary>
        public string Size { get; set; }
        /// <summary>
        /// 商品数量
        /// </summary>
        public int? Number { get; set; }

        /// <summary>
        /// 订单总价
        /// </summary>
        public decimal? Price { get; set; }

        /// <summary>
        /// 订单状态
        /// </summary>
        public int State { get; set; }
    }
}